package com.example.porfoliospring.controler;


import com.example.porfoliospring.model.*;
import com.example.porfoliospring.repository.*;
import com.example.porfoliospring.service.CompetenciaService;
import com.example.porfoliospring.service.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class BasicControler {
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private CompetenciaRepository competenciaRepository;
    @Autowired
    private ContactoRepository contactoRepository;
    @Autowired
    private ServicioRepository servicioRepository;
    @Autowired
    private GaleriaRepository galeriaRepository;

    @RequestMapping("/")
    public String mostrarHome(Model model) {
        Usuario usuario = usuarioRepository.findAll().get(0);
                // Asumiendo que el usuario con id 1 es el que queremos mostrar
        model.addAttribute("usuario", usuario);

        List<Competencia>competenciasLista =competenciaRepository.findAll();
        model.addAttribute("competenciasLista", competenciasLista);

        List<Servicio>serviciosLista =servicioRepository.findAll();
        model.addAttribute("serviciosLista", serviciosLista);

        List<Galeria> galeriaLista = galeriaRepository.findAll();
        model.addAttribute("galeriaLista", galeriaLista);

        return "PaginaPrincipal";
    }

    @PostMapping("/")
    public String procesarFormulario(@RequestParam("name") String name,
                                     @RequestParam("email") String email,
                                     @RequestParam("telefono") String telefono,
                                     @RequestParam("servicio") String servicio,
                                     @RequestParam("preferencia") String preferencia,
                                     Model model) {

        // Crear una nueva instancia de Contacto
        Contacto contacto = new Contacto();
        contacto.setNombre(name);
        contacto.setCorreoElectronico(email);
        contacto.setTelefono(telefono);
        contacto.setTipoServicioSolicitado(servicio);
        contacto.setPreferenciaContacto(preferencia);

        // Guardar la instancia en la base de datos
        contactoRepository.save(contacto);

        // Añadir la información a la vista para mostrarla al usuario
        model.addAttribute("mensaje", "Formulario enviado con éxito");
        model.addAttribute("contacto", contacto);

        // Volver a cargar la página principal con los datos actualizados


        Usuario usuario = usuarioRepository.findAll().get(0);
        // Asumiendo que el usuario con id 1 es el que queremos mostrar
        model.addAttribute("usuario", usuario);

        List<Competencia>competenciasLista =competenciaRepository.findAll();
        model.addAttribute("competenciasLista", competenciasLista);

        List<Servicio>serviciosLista =servicioRepository.findAll();
        model.addAttribute("serviciosLista", serviciosLista);

        List<Galeria> galeriaLista = galeriaRepository.findAll();
        model.addAttribute("galeriaLista", galeriaLista);


        return "PaginaPrincipal";
    }


    // */
}

/*
*un modelo por cada talbla menos la rara esa
un repo por cada modelo
un servicio por cada repositorio
solo un controlador

preguntas para paloma
lo del css si es ./ o ../
lo de llamar para poner cesar
lo de si hay que hacer post y algo mmas o que cosas. va el service?
get para index y post para el formulario
*
*
* lo del bucle
* https://www.baeldung.com/thymeleaf-iteration
*
* niidea
* https://vladmihalcea.com/the-best-way-to-use-the-manytomany-annotation-with-jpa-and-hibernate/
* */

