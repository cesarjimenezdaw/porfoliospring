package com.example.porfoliospring.repository;

import com.example.porfoliospring.model.Competencia;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompetenciaRepository extends JpaRepository<Competencia, Long> {
}
