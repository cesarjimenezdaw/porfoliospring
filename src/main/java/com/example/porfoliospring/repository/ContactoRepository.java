package com.example.porfoliospring.repository;

import com.example.porfoliospring.model.Contacto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactoRepository extends JpaRepository<Contacto, Long> {
}
