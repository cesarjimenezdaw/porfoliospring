package com.example.porfoliospring.repository;

import com.example.porfoliospring.model.Galeria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GaleriaRepository extends JpaRepository<Galeria, Long> {
}
