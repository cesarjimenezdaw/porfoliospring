package com.example.porfoliospring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PorfolioSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(PorfolioSpringApplication.class, args);
	}

}
