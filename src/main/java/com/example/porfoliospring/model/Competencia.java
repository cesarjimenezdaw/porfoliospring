package com.example.porfoliospring.model;

import jakarta.persistence.*;

@Entity
@Table(name="competencias")
public class Competencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_competencia")
    private Long id;

    @Column(name = "nombre_competencia")
    private String nombreCompetencia;

    @Column(name = "url_competencia")
    private String urlImagen;



    // Getters y setters
    // (Se omiten por brevedad)


    public Long getId() {
        return id;
    }

    public String getNombreCompetencia() {
        return nombreCompetencia;
    }

    public String getUrlImagen() {
        return urlImagen;
    }
}
