package com.example.porfoliospring.model;

import jakarta.persistence.*;

@Entity
//ojo que las cuenta como si fuese usuarios la tabla (si no existe la crea o algo asi raro)
@Table(name="usuarios")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "ocupacion")
    private String ocupacion;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "img_user")
    private String urlImagen;

    @Column(name = "sobremi")
    private String sobremi;

    @Column(name = "contacto_img")
    private String contacto_img;

    @Column(name = "correo")
    private String correo;

    @Column(name = "telefono")
    private String telefono;


    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public String getSobremi() {
        return sobremi;
    }

    public String getContacto_img() {
        return contacto_img;
    }

    public String getCorreo() {
        return correo;
    }

    public String getTelefono() {
        return telefono;
    }
}