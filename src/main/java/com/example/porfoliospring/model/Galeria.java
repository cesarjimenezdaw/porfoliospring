package com.example.porfoliospring.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "galeria")
public class Galeria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idGaleria;

    private String urlRepositorio;
    private String urlImagen;

    @ManyToMany
    @JoinTable(
            name = "galeria_competencia",
            joinColumns = @JoinColumn(name = "id_galeria"),
            inverseJoinColumns = @JoinColumn(name = "id_competencia")
    )
    private Set<Competencia> competencias;

    // Getters y setters

    public Long getIdGaleria() {
        return idGaleria;
    }

    public void setIdGaleria(Long idGaleria) {
        this.idGaleria = idGaleria;
    }

    public String getUrlRepositorio() {
        return urlRepositorio;
    }

    public void setUrlRepositorio(String urlRepositorio) {
        this.urlRepositorio = urlRepositorio;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public Set<Competencia> getCompetencias() {
        return competencias;
    }

    public void setCompetencias(Set<Competencia> competencias) {
        this.competencias = competencias;
    }
}
