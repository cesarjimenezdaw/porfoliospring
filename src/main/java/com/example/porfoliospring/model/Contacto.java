package com.example.porfoliospring.model;

import jakarta.persistence.*;

@Entity
@Table(name = "contacto")
public class Contacto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_contacto")
    private Integer idContacto;

    @Column(name = "nombre", nullable = false, length = 100)
    private String nombre;

    @Column(name = "correo_electronico", nullable = false, length = 100)
    private String correoElectronico;

    @Column(name = "telefono", length = 20)
    private String telefono;

    @Column(name = "tipo_servicio_solicitado", length = 100)
    private String tipoServicioSolicitado;

    @Column(name = "preferencia_contacto", length = 20)
    private String preferenciaContacto;

    // Getters and Setters

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipoServicioSolicitado() {
        return tipoServicioSolicitado;
    }

    public void setTipoServicioSolicitado(String tipoServicioSolicitado) {
        this.tipoServicioSolicitado = tipoServicioSolicitado;
    }

    public String getPreferenciaContacto() {
        return preferenciaContacto;
    }

    public void setPreferenciaContacto(String preferenciaContacto) {
        this.preferenciaContacto = preferenciaContacto;
    }
}
