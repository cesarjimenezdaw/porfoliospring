package com.example.porfoliospring.service;


import com.example.porfoliospring.model.Competencia;
import com.example.porfoliospring.repository.CompetenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompetenciaService {
    @Autowired
    private CompetenciaRepository compentenciaRepository;


    public Competencia getCompetenciaById(Long id) {
        return compentenciaRepository.findById(id).orElse(null);
    }
}

