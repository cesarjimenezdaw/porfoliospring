package com.example.porfoliospring.service;

import com.example.porfoliospring.model.Competencia;
import com.example.porfoliospring.model.Contacto;
import com.example.porfoliospring.repository.CompetenciaRepository;
import com.example.porfoliospring.repository.ContactoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactoService {
    @Autowired
    private ContactoRepository contactoRepository;


    public void saveContacto (Contacto cont) {
        contactoRepository.save(cont);
    }
}