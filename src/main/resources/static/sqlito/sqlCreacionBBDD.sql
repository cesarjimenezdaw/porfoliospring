DROP DATABASE IF EXISTS bbddPorfolioSpring;
CREATE DATABASE bbddPorfolioSpring;
USE bbddPorfolioSpring;

-- Tabla de Usuarios
CREATE TABLE usuarios (
    id_usuario INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100),
    apellidos VARCHAR(100),
    ocupacion VARCHAR(100),
    descripcion TEXT,
    img_user VARCHAR(100),
    sobremi TEXT
);

-- Tabla de Competencias
CREATE TABLE competencias (
    id_competencia INT AUTO_INCREMENT PRIMARY KEY,
    nombre_competencia VARCHAR(100),
    url_competencia VARCHAR(100)
);

-- Tabla de Servicios
CREATE TABLE servicios (
    id_servicio INT AUTO_INCREMENT PRIMARY KEY,
    nombre_servicio VARCHAR(100),
    descripcion_servicio TEXT
);

-- Tabla de Galería
CREATE TABLE galeria (
    id_imagenGaleria INT AUTO_INCREMENT PRIMARY KEY,
    url_imagenGaleria VARCHAR(255)
);

-- Tabla de Competencia-Lenguajes (relación muchos a muchos)
CREATE TABLE competencia_lenguajes (
    id_competencia INT,
    id_imagenGaleria INT,
    PRIMARY KEY (id_competencia, id_imagenGaleria),
    FOREIGN KEY (id_competencia) REFERENCES competencias(id_competencia),
    FOREIGN KEY (id_imagenGaleria) REFERENCES galeria(id_imagenGaleria)
);

-- Tabla de Contacto
CREATE TABLE contacto (
    id_contacto INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100),
    correo_electronico VARCHAR(100),
    telefono VARCHAR(20),
    tipo_servicio_solicitado VARCHAR(100),
    preferencia_contacto VARCHAR(20)
);

-- INSERTS --

-- Insert into usuarios
INSERT INTO usuarios (nombre, apellidos, ocupacion, descripcion, img_user, sobremi)
VALUES (
    'César',
    'Jiménez Diez',
    'Desarrollador fullstack',
    'Imagen de un portátil con una aplicación abierta y código CSS escrito',
    '../Imagenes/coding-1853305_640.jpg',
    'Descubre mi portfolio, soy César, un programador full-stack comprometido con la excelencia en el desarrollo de software.<br>
    Con experiencia en el desarrollo web, estoy comprometido a brindar soluciones tecnológicas innovadoras. Mi enfoque versátil y creativo se refleja en cada proyecto que emprendo.<br>
    Mi habilidad para crear aplicaciones robustas y atractivas se ha perfeccionado a lo largo de mi carrera. Desde el diseño frontend hasta la gestión de bases de datos en el backend, estoy preparado para abordar cualquier desafío que se presente.<br>
    La resolución eficiente de problemas y la entrega de productos de alta calidad son mis principales prioridades.<br>
    Estoy dedicado a mantenerme actualizado con las últimas tecnologías para ofrecer soluciones que destaquen en el mundo digital.'
);

-- Insert into competencias
INSERT INTO competencias (nombre_competencia, url_competencia)
VALUES 
    ('Java', '../Imagenes/java_274432.png'),
    ('C#', '../Imagenes/Cs.png'),
    ('Python', '../Imagenes/python-file_5815753.png'),
    ('PHP', '../Imagenes/php_12658497.png'),
    ('SQL', '../Imagenes/database_4248349.png'),
    ('HTML', '../Imagenes/html.png'),
    ('CSS', '../Imagenes/css_1961677.png');

-- Insert into servicios
INSERT INTO servicios (nombre_servicio, descripcion_servicio)
VALUES 
    ('Desarrollo de aplicaciones', 'Desarrollo de aplicaciones personalizadas para satisfacer tus necesidades, desde soluciones empresariales hasta aplicaciones móviles innovadoras. Transformo ideas en software funcional y eficiente.'),
    ('Diseño de páginas web', 'Diseño y creación de páginas web atractivas y funcionales. Desde la conceptualización hasta la implementación, trabajo para destacar la identidad de tu marca y ofrecer una experiencia de usuario óptima.'),
    ('Consultoría', 'Servicios de consultoría especializados para guiar tu empresa en la toma de decisiones estratégicas. Ofrezco asesoramiento experto en tecnología y procesos para impulsar el crecimiento y la eficiencia.');

-- Insert into galeria
INSERT INTO galeria (url_imagenGaleria)
VALUES 
    ('../Imagenes/im1.jpg'),
    ('../Imagenes/im2.jpg'),
    ('../Imagenes/im3.jpg'),
    ('../Imagenes/im4.jpg'),
    ('../Imagenes/im5.jpg');

-- Insert into competencia_lenguajes (assuming example relationships)
INSERT INTO competencia_lenguajes (id_competencia, id_imagenGaleria)
VALUES 
    (1, 1), -- Java and im1.jpg
    (2, 2), -- C# and im2.jpg
    (3, 3), -- Python and im3.jpg
    (4, 4), -- PHP and im4.jpg
    (5, 5); -- SQL and im5.jpg
