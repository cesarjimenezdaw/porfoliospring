drop database bbddPorfolioSpring;

create database bbddPorfolioSpring;
use bbddPorfolioSpring;

-- select * from contacto;

-- Tabla de Usuarios
CREATE TABLE usuarios (
    id_usuario INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100),
	apellidos varchar (100),
    ocupacion VARCHAR(100),
    descripcion TEXT,
    img_user varchar(100),
    sobremi text,
    contacto_img varchar(100),
    correo varchar(100),
    telefono varchar(100)   
);

-- Tabla de Competencias
CREATE TABLE competencias (
    id_competencia INT AUTO_INCREMENT PRIMARY KEY,
    nombre_competencia VARCHAR(100),
    url_competencia varchar(100)
);

-- Tabla de Servicios
CREATE TABLE servicios (
    id_servicio INT AUTO_INCREMENT PRIMARY KEY,
    nombre_servicio VARCHAR(100),
    descripcion_servicio TEXT
);

-- Tabla de Galería
CREATE TABLE galeria (
    id_galeria INT AUTO_INCREMENT PRIMARY KEY,
    url_repositorio varchar(255),
    url_imagen VARCHAR(255)
);
-- Tabla de Asociación Galería-Competencia
CREATE TABLE galeria_competencia (
    id_galeria INT,
    id_competencia INT,
    PRIMARY KEY (id_galeria, id_competencia),
    FOREIGN KEY (id_galeria) REFERENCES galeria(id_galeria) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (id_competencia) REFERENCES competencias(id_competencia) ON DELETE CASCADE ON UPDATE CASCADE
);


-- Tabla de Contacto
CREATE TABLE contacto (
    id_contacto INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100),
    correo_electronico VARCHAR(100),
    telefono VARCHAR(20),
    tipo_servicio_solicitado VARCHAR(100),
    preferencia_contacto VARCHAR(20)
);

INSERT INTO usuarios (nombre, apellidos, ocupacion, descripcion, img_user, sobremi, contacto_img, correo, telefono) VALUES 
('César', 
'Jiménez Diez',
'Desarrollador fullstack',
'Descubre mi portfolio, soy César, un programador full-stack comprometido con la excelencia en el desarrollo de software. Con experiencia en el desarrollo web, estoy comprometido a brindar soluciones tecnológicas innovadoras. Mi enfoque versátil y creativo se refleja en cada proyecto que emprendo. Mi habilidad para crear aplicaciones robustas y atractivas se ha perfeccionado a lo largo de mi carrera. Desde el diseño frontend hasta la gestión de bases de datos en el backend, estoy preparado para abordar cualquier desafío que se presente. La resolución eficiente de problemas y la entrega de productos de alta calidad son mis principales prioridades. Estoy dedicado a mantenerme actualizado con las últimas tecnologías para ofrecer soluciones que destaquen en el mundo digital.',
'/images/coding-1853305_640.jpg',
' Descubre mi portfolio, soy César, un programador full-stack comprometido con la excelencia en el desarrollo de software.<br>
Con experiencia en el desarrollo web, estoy comprometido a brindar soluciones tecnológicas innovadoras. Mi enfoque versátil y creativo se refleja en cada proyecto que emprendo.<br>
Mi habilidad para crear aplicaciones robustas y atractivas se ha perfeccionado a lo largo de mi carrera. Desde el diseño frontend hasta la gestión de bases de datos en el backend, estoy preparado para abordar cualquier desafío que se presente.<br>        
La resolución eficiente de problemas y la entrega de productos de alta calidad son mis principales prioridades. 
<br>Estoy dedicado a mantenerme actualizado con las últimas tecnologías para ofrecer soluciones que destaquen en el mundo digital.<br>',
'/images/5861840.png',
'cesarjimenezdaw@gmail.com',
'123456789'
);
 
INSERT INTO competencias (nombre_competencia, url_competencia) VALUES ('Java', '/images/java_274432.png');
INSERT INTO competencias (nombre_competencia, url_competencia) VALUES ('C#', '/images/Cs.png');
INSERT INTO competencias (nombre_competencia, url_competencia) VALUES ('Python', '/images/python-file_5815753.png');
INSERT INTO competencias (nombre_competencia, url_competencia) VALUES ('PHP', '/images/php_12658497.png');
INSERT INTO competencias (nombre_competencia, url_competencia) VALUES ('SQL', '/images/database_4248349.png');
INSERT INTO competencias (nombre_competencia, url_competencia) VALUES ('HTML', '/images/html.png');
INSERT INTO competencias (nombre_competencia, url_competencia) VALUES ('CSS', '/images/css_1961677.png');

-- Modificación de la tabla galeria para añadir el campo src
ALTER TABLE galeria
ADD COLUMN src VARCHAR(255);

-- Inserts actualizados con el nuevo campo src y la ruta relativa a /images
INSERT INTO galeria (url_repositorio, url_imagen) VALUES
('https://github.com/CESARJD/portfolio/blob/21d0af9453d69e352cc21908fa8f1e28506c7b21/prueba1', '/images/im1.jpg'),
('https://github.com/CESARJD/portfolio/blob/21d0af9453d69e352cc21908fa8f1e28506c7b21/prueba1', '/images/im2.jpg'),
('https://github.com/CESARJD/portfolio/blob/21d0af9453d69e352cc21908fa8f1e28506c7b21/prueba1', '/images/im3.jpg'),
('https://github.com/CESARJD/portfolio/blob/21d0af9453d69e352cc21908fa8f1e28506c7b21/prueba1', '/images/im4.jpg'),
('https://github.com/CESARJD/portfolio/blob/21d0af9453d69e352cc21908fa8f1e28506c7b21/prueba1', '/images/im5.jpg');



INSERT INTO servicios (nombre_servicio, descripcion_servicio) VALUES 
('Desarrollo de aplicaciones', 
 'Desarrollo de aplicaciones personalizadas para satisfacer tus necesidades, desde soluciones empresariales hasta aplicaciones móviles innovadoras. Transformo ideas en software funcional y eficiente.'),
('Diseño de paginas web', 
 'Diseño y creación de páginas web atractivas y funcionales. Desde la conceptualización hasta la implementación, trabajo para destacar la identidad de tu marca y ofrecer una experiencia de usuario óptima.'),
('Consultoria', 
 'Servicios de consultoría especializados para guiar tu empresa en la toma de decisiones estratégicas. Ofrezco asesoramiento experto en tecnología y procesos para impulsar el crecimiento y la eficiencia.');





INSERT INTO galeria_competencia (id_galeria, id_competencia) VALUES (1, 1), (1, 2), (1, 3);
INSERT INTO galeria_competencia (id_galeria, id_competencia) VALUES (2, 4), (2, 5);
INSERT INTO galeria_competencia (id_galeria, id_competencia) VALUES (3, 2), (3, 6);
INSERT INTO galeria_competencia (id_galeria, id_competencia) VALUES (4, 3), (4, 5), (4, 7);
INSERT INTO galeria_competencia (id_galeria, id_competencia) VALUES (5, 1), (5, 4), (5, 6);

